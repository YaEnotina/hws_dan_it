/* У папці banners лежить HTML код та папка з картинками.
При запуску програми на екрані має відображатись перша картинка.
Через 3 секунди замість неї має бути показано друга картинка.
Ще через 3 секунди – третя.
Ще через 3 секунди – четверта.
Після того, як будуть показані всі картинки – цей цикл має розпочатися наново.
Після запуску програми десь на екрані має з’явитись кнопка з написом Припинити.
Після натискання на кнопку Припинити цикл завершується, на екрані залишається показаною та картинка, яка була там при натисканні кнопки.
Поруч із кнопкою Припинити має бути кнопка Відновити показ, при натисканні якої цикл триває з тієї картинки, яка в даний момент показана на екрані.
Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.
Необов’язкове завдання підвищеної складності
При запуску програми на екрані повинен бути таймер з секундами та мілісекундами, що показує, скільки залишилося до показу наступної картинки.
Робити приховування картинки та показ нової картинки поступовим (анімація fadeOut/fadeIn) протягом 0.5 секунди. */


const banners = Array.from(document.querySelector('.images-wrapper').children);
let activeSlide = 0;
let timerInterval;
let eventInterval = true;

let btnStartStop = document.createElement('button');
btnStartStop.style.position = 'absolute';
btnStartStop.textContent = 'Припинити';
document.body.appendChild(btnStartStop);

changeSlide(activeSlide)
startSlider(eventInterval);

btnStartStop.addEventListener('click', () => {
    if (eventInterval) {
        eventInterval = false
        startSlider(eventInterval)
        btnStartStop.textContent = 'Відновити показ'
    }
    else if (!eventInterval) {
        eventInterval = true
        startSlider(eventInterval)
        btnStartStop.textContent = 'Припинити';
    }
})
function startSlider(eventInterval) {
    if (eventInterval) {
        timerInterval = setInterval(() => {
            changeSlide(activeSlide)
        }, 1000)
    }
    if (!eventInterval) {
        clearInterval(timerInterval)
    }
}
function changeSlide(id) {
    banners.forEach((element, index) => {
        if (index !== id) {
            element.style.display = 'none';
        }
        if (index === id) {
            element.style.display = 'block';
        }

    });
    if (activeSlide === banners.length - 1) {
        activeSlide = 0
    }
    else {
        activeSlide++
    }


}




