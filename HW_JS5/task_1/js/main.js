// Написати функцію createNewUser(), яка буде створювати та повертати об’єкт newUser.
// При виклику функція повинна запитати ім’я та прізвище.
// Використовуючи дані, введені юзером, створити об’єкт newUser з властивостями firstName та lastName.
// Додати в об’єкт newUser метод getLogin(), який повертатиме першу літеру імені юзера, з’єднану з прізвищем, все в нижньому регістрі (наприклад, Ivan Kravchenko → ikravchenko).
// Створити юзера за допомогою функції createNewUser(). Викликати у цього юзера функцію getLogin(). Вивести у консоль результат виконання функції.
// Необов’язкове завдання підвищеної складності
// Зробити так, щоб властивості firstName та lastName не можна було змінювати напряму. Створити функції-сеттери setFirstName() та setLastName(), які дозволять змінити дані властивості.
function createNewUser() {
    let newUser = {};
    newUser.firstName = prompt("Введіть ім'я");
    newUser.lastName = prompt("Введіть прізвище");
    Object.defineProperties(newUser, {
        firstName: {
            configurable: true,
            writable: false
        },
        lastName: {
            configurable: true,
            writable: false
        }
    });

    newUser.getLogin = function () {
        return (this.firstName[0] + this.lastName).toLowerCase();
    }
    newUser.setFirstName = function (firstName) {
        Object.defineProperty(this, 'firstName', {
            writable: true
        })
        this.firstName = firstName;
        return this.firstName

    }
    newUser.setLastName = function (lastName) {
        Object.defineProperty(this, 'lastName', {
            writable: true
        })
        this.lastName = lastName;
        return this.lastName
    }
    return newUser;
}
// console.log(createNewUser());
console.log(createNewUser().getLogin());
// console.log(createNewUser().setFirstName('Ivan'));
// console.log(createNewUser().setLastName('Vata'));


