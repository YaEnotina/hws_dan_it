let firstNum, secondNum, operator;
// form for input users data
do {
    firstNum = prompt('Enter first number', firstNum);
    secondNum = prompt('Enter second number', secondNum);
    operator = prompt('Enter command', operator);
} while (!check(firstNum, secondNum, operator));

//function for check correct user input value
function check(firstNum, secondNum, operator) {
    if (!isNaN(firstNum) && !isNaN(secondNum) && (operator === '*' || operator === '-' || operator === '+' || operator === '/')) {
        return true;
    }
    return false;
}
// function for calculeting user input values
function userResult(firstNum, secondNum, command) {
    firstNum = Number(firstNum);
    secondNum = Number(secondNum);
    if (command == '*') {
        return firstNum * secondNum;
    }
    if (command == '-') {
        return firstNum - secondNum;
    }
    if (command == '+') {
        return firstNum + secondNum;
    }
    if (command == '/') {
        return firstNum / secondNum;
    }
}
// printing the result to the console
console.log((userResult(firstNum, secondNum, operator)));