/* Завдання
Реалізувати функцію підсвічування клавіш.
Технічні вимоги:
У файлі index.html лежить розмітка для кнопок.
Кожна кнопка містить назву клавіші на клавіатурі
Після натискання вказаних клавіш – та кнопка, на якій написана ця літера, повинна фарбуватися в синій колір. При цьому якщо якась інша літера вже раніше була пофарбована в синій колір – вона стає чорною. Наприклад за натисканням Enter перша кнопка забарвлюється у синій колір. Далі, користувач натискає S, і кнопка S забарвлюється в синій колір, а кнопка Enter знову стає чорною. */

const buttonWrapper = document.querySelector('.btn-wrapper');
const buttonArray = [...buttonWrapper.children].map(element => {
    return element.textContent.toLowerCase()
});

let previosKey;
const previosKeyColor = '#000000'
const activeKeyColor = 'blue'
document.addEventListener('keyup', function (event) {
    activeKey = event.key.toLowerCase()
    if (keyCheck(activeKey, buttonArray)) {
        if (activeKey !== previosKey) {
            changeColor(buttonArray.indexOf(activeKey), activeKeyColor)
            changeColor(buttonArray.indexOf(previosKey), previosKeyColor)
            previosKey = activeKey
        }
    }
})
function changeColor(changeEl, colorEl) {
    if (changeEl >= 0) {
        console.log(changeEl);
        buttonWrapper.children[changeEl].style.backgroundColor = colorEl //`${colorEl}`
    }
}
function keyCheck(activeKey, defaultKeys) {
    if (defaultKeys.includes(activeKey)) {
        return true
    }
    return false
}