let userNumber, tempUserNumber;
let userFound = false;
do {
    userNumber = prompt('Enter your number');
} while (isNaN(userNumber) || userNumber <= 0);

for (let index = userNumber; index > 0; index--) {
    if (index % 5 === 0) {
        console.log(index);
        userFound = true;
    }
}
if (!userFound) {
    console.log('Sorry, no numbers');
}
