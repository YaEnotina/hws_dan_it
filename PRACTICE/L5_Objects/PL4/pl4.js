/*
 * Завдання 4.
 *
 *  Створити об'єкт danItStudent,
 * У якого такі властивості: ім'я, прізвище, кількість зданих домашніх работ.
 *
 * Запитати у користувача "Що ви хочете дізнатися про студента?"
 * Якщо користувач запровадив " name " чи " ім'я " - вивести у консоль ім'я студента.
 * Якщо користувач ввів "lastName" або "прізвище" - вивести до консольпрізвища студента.
 * Якщо користувач запровадив " оцінка " чи " homeworks " - вивести у консоль кількість зданих робіт.
 * Якщо ввів, щось не з перерахованого - вивести в консоль - "Вибачте таких даних немає"
 */
let someKey;
let userPromt = prompt('Entr data');

if (userPromt == 'name' || userPromt == 'ім\'я') {
    someKey = 'firstName';
}
else if (userPromt == 'lastName' || userPromt == 'прізвище') {
    someKey = 'secondName';
}
else if (userPromt == 'homeworks' || userPromt == 'оцінка') {
    someKey = 'homeWorks';
} else {
    console.log('Вибачте таких даних немає');
}

const danItStudent = {
    firstName: 'Ave',
    secondName: 'Divine',
    homeWorks: 20,
    someDataAsk: function (someKey) {
        if (someKey in danItStudent) {
            console.log(this[someKey]);
        }
        else {
            console.log('error');
        }
    }
}
danItStudent.someDataAsk(someKey);