/**
 * Завдання 2.
 *
 * Розширити функціонал об'єкта з попереднього завдання:
 * - Метод sayHi повинен вивести повідомлення: «Привіт. Мене звуть Ім'я Прізвище.»;
 * - Додати метод, який змінює значення зазначеної властивості об'єкта.
 *
 * Просунута складність:
 * Метод має бути «розумним» — він генерує помилку під час спроби
 * Зміни значення неіснуючого в об'єкті якості.
 */

const objectUser = {
    firstName: 'Alan',
    secondName: 'Boris',
    proffecion: 'Developer',
    // sayHi: function () {
    //     return `Привет ${this.firstName} ${this.secondName}`;
    // },
    changeSomething: function (userKey, value) {
        if (userKey in objectUser) {
            this[userKey] = value;
        }
        else {
            alert('error');
            
        }
    }

}
objectUser.changeSomething('proffecion', 'exDeveloper');
console.log(objectUser);