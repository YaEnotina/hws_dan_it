/**
 * Завдання 3.
 *
 * Розширити функціонал об'єкта з попереднього завдання:
 * - Додати метод, який додає об'єкту нову властивість із зазначеним значенням.
 *
 * Просунута складність:
 * Метод має бути «розумним» — він генерує помилку при створенні нової властивості
 * Властивість з таким ім'ям вже існує.
 */
const objectUser = {
    firstName: 'Alan',
    secondName: 'Boris',
    proffecion: 'Developer',
    sayHi: function () {
        return `Привет ${this.firstName} ${this.secondName}`;
    },
    changeSomething: function (userKey, value) {
        if (userKey in objectUser) {
            this[userKey] = value;
        }
        else {
            alert('error');
        }
    },
    createSomething: function(createKey,value){
        if (createKey in objectUser) {
            alert('Error create')
        }
        else{
            this[createKey] = value;
        }
    }

}
objectUser.changeSomething('proffecion', 'exDeveloper');
objectUser.createSomething('age', 26);
console.log(objectUser);