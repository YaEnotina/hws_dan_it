/**
 * Завдання 1.
 *
 * Створити об'єкт користувача, який має три властивості:
 * - Ім'я;
 * - Прізвище;
 * - Професія.
 *
 * А також одним методом sayHi, який виводить у консоль повідомлення 'Привіт.'.
 */
const objectUser = {
    firstName: 'Alan',
    secondName: 'Boris',
    proffecion: 'Developer',
    sayHi: function (){
        return `Привет ${this.firstName}`;
    }

}
console.log(objectUser.sayHi());