/**
 * Завдання 6.
 *
 * За допомогою циклу for...in вивести в консоль усі властивості
 * першого рівня об'єкта у форматі «ключ-значення».
 *
 * Просунута складність:
 * Поліпшити цикл так, щоб він умів виводити властивості об'єкта другого рівня вкладеності.
 */

/* Дано */
const user = {
    firstName: "Walter",
    lastName: "White",
    job: "Programmer",
    pets: {
      cat: "Kitty",
      dog: "Doggy",
    },
  };
  for (const key in user) {
    if (typeof user[key] ==='object'){
        console.log(`Object : ${key}`);
        for (const key2 in user[key])
        console.log(`${key2}`);
    }
    console.log(`${key}`);
        
    
  }