/**
 * Завдання 8.
 *
 * Написати функцію-фабрику, яка повертає об'єкти користувачів.
 *
 * Об'єкт користувача має три властивості:
 * - Ім'я;
 * - Прізвище;
 * - Професія.
 *
 * Функція-фабрика в свою чергу має три параметри,
 * які відбивають вищеописані властивості об'єкта.
 * Кожен параметр функції має значення за замовчуванням: null.
 */

// function objectUser(firstName = null, seconName = null, profession = null) {
//     return {
//         firstName,
//         seconName,
//         profession,
//     }
// }
// const user = objectUser('Ya', 'Enotina', 'Raccoon');
// console.log(user);


// function objectUser1(firstName = null, seconName = null, profession = null) {
//     const user = {
//         firstName,
//         seconName,
//         profession,
//     }
//     return user

// }
// const user1 = objectUser1('Enotina', 'Ya', 'Raccoon');
// console.log(user1);

let calculator = {
    firstNum : 4,
    secondNum: 5,
    sum(firstNum, secondNum) {
        return this.firstNum + this.secondNum
    }

}

console.log(calculator.sum());





















