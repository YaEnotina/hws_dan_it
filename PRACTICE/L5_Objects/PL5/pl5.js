/*
 * Завдання 5.
 *
 *  Запитати в користувача ім'я, прізвище, вік (перевірити на правильну цифру
 * і що число між 15 і 100), чи є діти, якого кольору очі.
 * Усі ці дані записати в об'єкт.
 * Перебрати об'єкт і вивести у консолі фразу
 * `Властивість ${name}: ${значення name}` і так з усіма властивостями
 */
let age, firstName = 'Max', secondName = 'Sergeev', kids = 'yes', eye = 'blue';
do {
    age = 52;
    console.log(age);
} while (Number(age) < 15  && Number(age) > 100);


const userData = {

}
userData.age = age
userData.firstName = firstName
userData.secondName = secondName
userData.kids = kids
userData.eye = eye

for (const iterator in userData) {
    console.log(`${iterator} ${userData[iterator]}`);
    
}

console.log(userData);