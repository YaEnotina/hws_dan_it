/**
 * Завдання 7.
 *
 * Написати функцію-помічник для ресторану.
 *
 * Функція має два параметри:
 * - Розмір замовлення (small, medium, large);
 * - Тип обіду (breakfast, lunch, dinner).
 *
 * Функція повертає об'єкт із двома полями:
 * - totalPrice - загальна сума страви з урахуванням її розміру та типу;
 * - totalCalories — кількість калорій, що міститься у блюді з урахуванням його розміру та типу.
 *
 * Нотатки:
 * - Додаткові перевірки робити не потрібно;
 * - У рішенні використовувати референтний об'єкт з цінами та калоріями.
 */

/* Дано */
const priceList = {
    sizes: {
      small: {
        price: 15,
        calories: 250,
      },
      medium: {
        price: 25,
        calories: 340,
      },
      large: {
        price: 35,
        calories: 440,
      },
    },
    types: {
      breakfast: {
        price: 4,
        calories: 25,
      },
      lunch: {
        price: 5,
        calories: 5,
      },
      dinner: {
        price: 10,
        calories: 50,
      },
    },
  };
  function sum (size,type){
    const totalData = {
    totalPrice : priceList.sizes[size].price + priceList.types[type].price,
    totalCalories : priceList.sizes[size].calories + priceList.types[type].calories
    }
    return totalData
  }
  
  console.log(sum('small','dinner'))