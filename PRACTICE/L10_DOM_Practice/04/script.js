/**
 * Завдання 4.
 *
 * Отримати елемент із класом .list-item.
 * Відібрати елемент із контентом: «Item 5».
 *
 * Замінити текстовий вміст цього елемента на посилання, вказане в розділі «дано».
 *
 * Зробити це так, щоб новий елемент у розмітці не було створено.
 *
 * Потім відібрати елемент із контентом: «Item 6».
 * Замінити вміст цього елемента на таке ж посилання.
 *
 * Зробити це так, щоб у розмітці було створено новий елемент.
 *
 * Умови:
 * - обов'язково використовувати метод для перебору;
 * - Пояснити різницю між типом колекцій: Array та NodeList.
 */

/* Дано */
const targetElement =
  '<a href="https://www.google.com" target="_blank" rel="noreferrer noopener">Google it!</a>';



let contextItem5 = Array.from(document.querySelectorAll('.list-item'));
console.log(contextItem5);
contextItem5.forEach(function (elem) {
  if (elem.innerText === 'Item 5') {
    elem.innerHTML = '<a href="https://www.google.com" target="_blank" rel="noreferrer noopener">Google it!</a>';
  }
  if (elem.innerText === 'Item 6') {
    elem.outerHTML = '<a href="https://www.google.com" target="_blank" rel="noreferrer noopener">Google it!!</a>';
  }
  
})