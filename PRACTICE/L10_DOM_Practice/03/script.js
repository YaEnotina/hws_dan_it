/**
 * Завдання 3.
 *
 * На екрані вказано список товарів із зазначенням назви та кількості на складі.
 *
 * Знайти товари, які закінчилися та:
 * - Змінити 0 на «закінчився»;
 * - Змінити колір тексту на червоний;
 * - Змінити жирність тексту на 600.
 *
 * Вимоги:
 * - Колір елемента змінити за допомогою модифікації атрибуту style.
 */

let arr = Array.from(document.querySelector('ul').children).map(function (elem) {

    return elem.innerHTML;
});
let newArr = [];
arr.forEach(function (elem) {
console.log(elem.length);
    let separator = elem.indexOf(':');
    console.log(separator);
    newArr.push([elem.substring(0, separator+1), elem.substring(separator+1, elem.length)]);
    // console.log(newArr);
    return newArr
})
for (let i = 0; i < newArr.length; i++) {
    if (newArr[i][1] === ' 0') {
        newArr[i][1] = ' закінчився';
        document.querySelector('ul').children[i].innerText = newArr[i][0] + '' + newArr[i][1];
        document.querySelector('ul').children[i].style.color = 'red';
        document.querySelector('ul').children[i].style.fontWeight = 'bold';
    }
}
// console.log('Arr' + arr);
console.log(newArr);