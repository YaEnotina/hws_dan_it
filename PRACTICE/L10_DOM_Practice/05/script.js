/**
 * Завдання 5.
 *
 * Написати програму для редагування кількості залишку продуктів на складі магазину.
 *
 * Програма має запитувати назву товару для редагування.
 * Якщо введеного товару на складі немає - програма проводить повторний запит назви товару
 * доти, доки відповідну назву не буде введено.
 *
 * Після чого програма запитує нову кількість товару.
 * Після чого програма вносить зміни на веб-сторінку: замінює залишок зазначеного товару на нову кількість.
 */
