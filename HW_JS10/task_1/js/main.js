/* У папці tabs лежить розмітка для вкладок. Потрібно, щоб після натискання на вкладку відображався конкретний текст для потрібної вкладки. При цьому решта тексту повинна бути прихована. У коментарях зазначено, який текст має відображатися для якої вкладки.
Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.
Потрібно передбачити, що текст на вкладках може змінюватись, і що вкладки можуть додаватися та видалятися. При цьому потрібно, щоб функція, написана в джаваскрипті, через такі правки не переставала працювати. */


// Не изменяя HTML =) Так интереснее)
const menuButtons = document.querySelector('.tabs');
const menuTabs = document.querySelectorAll('.tabs-content >li');
if (menuButtons.children.length !== menuTabs.length) {
    document.body.innerHTML = 'Error in menuButtons.children.length and menuTabs.length';
}
let activeTab = 0
console.log(menuButtons.children.length);
console.log(menuTabs.length);

document.addEventListener('readystatechange', () => {

    invisibleContext();
    menuButtons.children[activeTab].classList.add('active');
    menuTabs[activeTab].style.display = 'block'
})
menuButtons.addEventListener('click', handlerClick => {
    activeTab = [...menuButtons.children].indexOf(handlerClick.target)
    invisibleContext();
    handlerClick.target.classList.add('active');
    menuTabs[activeTab].style.display = 'block'
})
function invisibleContext() {
    [...menuButtons.children].forEach(element => {
        element.classList.remove('active');
        // console.log(element);
        menuTabs.forEach(tab => {
            tab.style.display = 'none'
        })

    });
}

