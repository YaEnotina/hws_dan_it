/* Візьміть виконане домашнє завдання номер 5 (створена вами функція createNewUser()) і доповніть її наступним функціоналом:
При виклику функція повинна запитати дату народження (текст у форматі dd.mm.yyyy) і зберегти її в полі birthday.
Створити метод getAge() який повертатиме скільки користувачеві років.
Створити метод getPassword(), який повертатиме першу літеру імені користувача у верхньому регістрі, з’єднану з прізвищем (у нижньому регістрі) та роком народження. (наприклад, Ivan Kravchenko 13.03.1992 → Ikravchenko1992.
Вивести в консоль результат роботи функції createNewUser(), а також функцій getAge() та getPassword() створеного об’єкта. */
function createNewUser() {
    let newUser = {};
    // For DEBUG
    newUser.firstName = 'Yan';
    newUser.lastName = 'Kravchenko';
    newUser.birthday = '20.06.1984';
    //* newUser.firstName = prompt("Введіть ім'я");
    //* newUser.lastName = prompt("Введіть прізвище");
    //* newUser.birthday = prompt("Введіть дату народження");
    Object.defineProperties(newUser, {
        firstName: {
            configurable: true,
            writable: false
        },
        lastName: {
            configurable: true,
            writable: false
        }
    });
    newUser.getAge = function () {
        let birthdayYear = this.birthday.slice(-4);
        let birthdayMonth = this.birthday.slice(3, 5);
        let birthdayDay = this.birthday.slice(0, 2);
        let userAge = new Date().getFullYear() - birthdayYear;
        //!Вырвиглаз. Это нужно оптимизировать для читабельности.
        if (birthdayMonth > (new Date().getMonth()) + 1) {
            return userAge--;
        }
        else if (birthdayMonth == (new Date().getMonth()) + 1) {
            if (birthdayDay <= new Date().getDate()) {
                return userAge--;
            }
            else {
                return userAge;
            }
        }
        else {
            return userAge;
        }
    }
    newUser.getLogin = function () {
        return this.firstName[0] + this.lastName.toLowerCase();
    }
    newUser.getPassword = function () {
        return this.firstName[0] + this.lastName.toLowerCase() + this.birthday.slice(-4);
    }
    newUser.setFirstName = function (firstName) {
        Object.defineProperty(this, 'firstName', {
            writable: true
        })
        this.firstName = firstName;
        return this.firstName

    }
    newUser.setLastName = function (lastName) {
        Object.defineProperty(this, 'lastName', {
            writable: true
        })
        this.lastName = lastName;
        return this.lastName
    }
    return newUser;
}
console.log(createNewUser());
console.log(createNewUser().getLogin());
console.log(createNewUser().getPassword());
console.log(createNewUser().getAge());


