//* Знайти всі параграфи на сторінці та встановити колір фону #ff0000
let allParagraphs = document.querySelectorAll('p');
allParagraphs.forEach(allParagraphs => {
    allParagraphs.style.backgroundColor = '#ff0000';
});

//* Знайти елемент із id=”optionsList”. Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
let findedElement = document.getElementById('optionsList');
console.log(findedElement);
let parentElem = document.getElementById('optionsList').parentElement;
console.log(parentElem);
let childElem = Array.from(document.getElementById('optionsList').childNodes); //! Тут мне непонятно из задания что нужно вернуть. Если неправильно понял - то нужно .children юзать.
console.log(childElem);
if (childElem) {
    childElem.forEach(element => {
        console.log('Название:', element.nodeName, 'Тип:', element.nodeType);
    });
}
//* Встановіть в якості контента елемента з класом testParagraph наступний параграф – This is a paragraph
document.querySelector('#testParagraph').innerText = 'This is a paragraph';

//* Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.
let mainHeaderChildren = Array.from(document.querySelector('.main-header').children);
console.log(mainHeaderChildren);
mainHeaderChildren.forEach(element => {
    element.classList.add('nav-item');
})

//* Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.
let delSectionTitle = Array.from(document.querySelectorAll('.section-title'));
delSectionTitle.forEach(element => {
    element.classList.remove('section-title');
})
