const html = document.documentElement;
console.log(html);
// html.dataset.theme = 'light';

if (localStorage.getItem('theme') === null){
    localStorage.setItem('theme', 'light');
}
else {
    let themeState = localStorage.getItem('theme');
    console.log(themeState);
    html.dataset.theme = `${themeState}`;
}
    // html.dataset.theme = 'dark';

let buttonText;
const switcher = document.querySelector('.switcher');
switcher.textContent=(localStorage.getItem('theme')).toUpperCase();
switcher.addEventListener('click', () => {
    console.log('click');
    themeState = localStorage.getItem('theme')
    if(themeState ==='dark'){
        buttonText= (localStorage.getItem('theme')).toUpperCase();
        
        changeTheme('light',buttonText);
        
    }
    else{
        buttonText= (localStorage.getItem('theme')).toUpperCase();
        html.dataset.theme = 'light';
        localStorage.setItem('theme', 'light');
        changeTheme('dark',buttonText)
    }
    

    
})

function changeTheme(themeState,buttonText) {
    html.dataset.theme = `${themeState}`;
    localStorage.setItem('theme', `${themeState}`);
    switcher.textContent=`${buttonText}`;
}