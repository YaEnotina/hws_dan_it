// Функція на вхід приймає три параметри:
// масив із чисел, що становить швидкість роботи різних членів команди. Кількість елементів у масиві означає кількість людей у команді. Кожен елемент означає скільки стор поінтів (умовна оцінка складності виконання завдання) може виконати даний розробник за 1 день.
// масив з чисел, який є беклогом (список всіх завдань, які необхідно виконати). Кількість елементів у масиві означає кількість завдань у беклозі. Кожне число в масиві означає кількість сторі поінтів, необхідні виконання даного завдання.
// Дата дедлайну (об’єкт типу Date).
// Після виконання, функція повинна порахувати, чи команда розробників встигне виконати всі завдання з беклогу до настання дедлайну (робота ведеться починаючи з сьогоднішнього дня). Якщо так, вивести на екран повідомлення: Усі завдання будуть успішно виконані за ? днів до настання дедлайну!. Підставити потрібну кількість днів у текст. Якщо ні, вивести повідомлення Команді розробників доведеться витратити додатково ? годин після дедлайну, щоб виконати всі завдання в беклозі
// Робота триває по 8 годин на день по будніх днях


let commandArr = [1, 1, 1,1]
let backlogArr = [2, 6, 4, 5, 3, 4, 2, 1, 5, 4, 6, 1, 2, 3, 4]
const endDedlineDate = new Date("2023-07-07T23:59:00");
const startDeadlineDate = new Date();
console.log(startDeadlineDate);

let days = workingDays(startDeadlineDate, endDedlineDate);
console.log(days);

console.log(startDeadlineDate.getTime());
console.log(endDedlineDate.getTime());

let storyPointBacklog = summStoryPoints(backlogArr);
console.log(storyPointBacklog);

let storyPointsCommnd = summStoryPoints(commandArr) * days;
console.log(storyPointsCommnd);
//вычисление рабочих дней недели.
function workingDays(startDeadlineDate, endDedlineDate) {
    let workingDays = 0;
    console.log(startDeadlineDate,typeof(startDeadlineDate));
    while (startDeadlineDate <= endDedlineDate) {
        if (startDeadlineDate.getDay() != 6 && startDeadlineDate.getDay() != 0) {
            workingDays++;
        }
        startDeadlineDate.setDate(startDeadlineDate.getDate() + 1);
        
    }
    return workingDays
}

function summStoryPoints(arr) {
    return arr.reduce((sum, el) => {
        return sum + el
    })
}
function answer(backLog, command) {
    if ((command - backLog) < 0) {
        let needDays = Math.ceil(Math.abs((command - backLog)/9*8));
        // console.log(command - backLog);
        return `Команді розробників доведеться витратити додатково ${needDays} годин після дедлайну, щоб виконати всі завдання в беклозі`
    } else {
        // console.log(command - backLog);
        return `Усі завдання будуть успішно виконані за ${Math.ceil((command - backLog)/9)} днів до настання дедлайну!.`
    }
}

console.log(answer(storyPointBacklog, storyPointsCommnd));

// console.log(endDedlineDate, startDeadlineDate);
// сумма массива через
//function summStoryPoints(arr) {
//     let commandStoryPoints = 0;
//     arr.forEach((el) => {
//         commandStoryPoints += el;
//     })
//     return commandStoryPoints
// }
//Cумма через reduce