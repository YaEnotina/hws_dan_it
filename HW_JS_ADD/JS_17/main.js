// Написать функцию для рекурсивного полного клонирования объекта (без единой передачи по ссылке, внутренняя вложенность свойств объекта может быть достаточно большой).
// Функция должна успешно копировать свойства в виде объектов и массивов на любом уровне вложенности.
// В коде нельзя использовать встроенные механизмы клонирования, такие как функция Object.assign() или спред-оператор.

const priceList = {
    sizes: {
      small: {
        price: 15,
        calories: 250,
      },
      medium: {
        price: 25,
        calories: 340,
      },
      large: {
        price: 35,
        calories: 440,
      },
    },
    types: {
      breakfast: {
        price: 4,
        calories: 25,
      },
      lunch: {
        price: 5,
        calories: 5,
      },
      dinner: {
        price: 10,
        calories: 50,
      },
    },
  };

function clone(obj) {
  const copy = Array.isArray(obj) ? [] : {};
  for (const key in obj) {
    copy[key] = clone(obj[key]);
  }
  return copy;
}
console.log(priceList);
console.log(clone(priceList));